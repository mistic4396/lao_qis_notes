



## vagrant+virtualbox

### vagrant

> 虚拟机配置生成

01 访问Vagrant官网
	https://www.vagrantup.com/
02 点击Download
Windows，MacOS，Linux等
03 选择对应的版本
04 傻瓜式安装
05 命令行输入**vagrant**，测试是否安装成功

### virtual box

> 虚拟机基础

01 访问VirtualBox官网
https://www.virtualbox.org/
02 选择左侧的“Downloads”
03 选择对应的操作系统版本
04 傻瓜式安装
05 [win10中若出现]安装virtualbox快完成时立即回滚，并提示安装出现严重错误
(1)打开服务
(2)找到Device Install Service和Device Setup Manager，然后启动
(3)再次尝试安装

## 安装centos7

1. 创建centos7文件夹，并进入其中[目录全路径不要有中文字符]

2. 将virtualbox.box文件添加到vagrant管理的镜像中

   ```
   (1)下载网盘中的virtualbox.box文件
   (2)保存到磁盘的某个目录，比如D:\virtualbox.box
   (3)添加镜像并起名叫centos/7：vagrant box add centos/7 D:\virtualbox.box
   (4)vagrant box list 查看本地的box[这时候可以看到centos/7]02 在此目录下打开cmd，
   ```

3. 运行 `vagrant init centos/7`，此时会在当前目录下生成Vagrantfile

4. 启动创建虚拟机

```
在此目录打开cmd窗口，执行vagrant up[打开virtual box观察，可以发现centos7创建成
```





05 以后大家操作虚拟机，还是要在centos文件夹打开cmd窗口操作



```
vagrant halt 优雅关闭
vagrant up 正常启动
vagrant reload  修改了Vagrantfile，要想使正常运行的centos7生效
vagrant ssh 进入刚才创建的centos7中
vagrant status 查看centos7的状态
vagrant destroy 删除centos7

```



### Xshell连接centos7

01 使用centos7的默认账号连接
在centos文件夹下执行vagrant ssh-config
关注:Hostname Port IdentityFile
IP:127.0.0.1
port:2222
用户名:vagrant
密码:vagrant
文件:Identityfile指向的文件private-key

> F:\centos\DockerCentos7\.vagrant\machines\default\virtualbox

02 使用root账户登录

1. vagrant ssh 进入到虚拟机中

2. sudo -i

3. vi /etc/ssh/sshd_config

4. 修改PasswordAuthentication yes

   ```
   vi test.txt
   按a或i进入编辑模式，输入 I am a boy
   然后按esc键退出编辑模式，输入:wq保存并退出。
   ```

   

5. 修改密码

   ```
   passwd
   [root@localhost ~]# passwd
   Changing password for user root.
   New password:
   ```

   

6. systemctl restart sshd [重启sshd服务]

   



### box的打包分发

01 退出虚拟机
`vagrant halt`
02 打包
`vagrant package --output first-docker-centos7.box`
03 得到first-docker-centos7.box
04 将first-docker-centos7.box添加到其他的vagrant环境中
`vagrant box add first-docker-centos7 first-docker-centos7.box`
05 得到Vagrantfile
vagrant init first-docker-centos7
06 根据Vagrantfile启动虚拟机
vagrant up [此时可以得到和之前一模一样的环境，但是网络要重新配置]

## 安装docker

01 进入centos7
`vagrant ssh`
02 卸载之前的docker
`sudo yum remove docker \`
`docker-client \`
`docker-client-latest \`
`docker-common \`
`docker-latest \`
`docker-latest-logrotate \`
`docker-logrotate \`
`docker-engine`
03 安装必要的依赖
`sudo yum install -y yum-utils \`
`device-mapper-persistent-data \`

`lvm2`

04 设置docker仓库 [设置阿里云镜像仓库可以先自行百度，后面课程也会有自己的docker hub讲解]
`sudo yum-config-manager \`
`--add-repo \`
`https://download.docker.com/linux/centos/docker-ce.repo`
[访问这个地址，使用自己的阿里云账号登录，查看菜单栏左下角，发现有一个镜像加速
器:https://cr.console.aliyun.com/cn-hangzhou/instances/mirrors]
05 安装docker
`sudo yum install -y docker-ce docker-ce-cli containerd.io`
06 启动docker
`sudo systemctl start docker`
07 测试docker安装是否成功
`sudo docker run hello-world`

## docker基本体验

01 创建tomcat容器
`docker pull tomcat`
`docker run -d --name my-tomcat -p 9090:8080 tomcat`
02 创建mysql容器

```
docker run -d --name my-mysql -p 3301:3306 -e MYSQL_ROOT_PASSWORD=jack123 --privileged
mysql
```

03 进入到容器里面

03 进入到容器里面

```shell
docker exec -it containerid /bin/bash
```

```shell
docker pull 拉取镜像到本地
docker run 根据某个镜像创建容器
-d 让容器在后台运行，其实就是一个进程
--name 给容器指定一个名字
-p 将容器的端口映射到宿主机的端口
docker exec -it 进入到某个容器中并交互式运行
```

## Dockerfile

### 语法

```dockerfile
1//指定基础镜像
FROM ubuntu:14.04 
2//设置变量的值 格式：【ENV key value】 使用：${MYSQL_MA JOR} 动态修改：docker run --e key=value
ENV MYSQL_MAJOR 5.7
3//设置镜像标签
LABEL email="itcrazy2016@163.com"
4//指定数据的挂在目录
VOLUME /var/lib/mysql
5//复制 目录不存在会自动创建
COPY docker-entrypoint.sh /usr/local/bin/
6//复制压缩文件提取和解压
ADD application.yml /etc/itcrazy2016/
7//指定镜像的工作目录，之后的命令都是基于此目录工作，若不存在则创建
//会在/usr/local/tomcat下创建test.txt文件
WORKDIR /usr/local
WORKDIR tomcat
RUN touch test.txt
//会在/usr/local/tomcat下创建test.txt文件
WORKDIR /root
ADD app.yml test/
8//容器启动的时候默认会执行的命令，若有多个CMD命令，则最后一个生效
CMD ["mysqld"] 或 CMD mysqld
9//在镜像内部执行一些命令，比如安装软件，配置环境等
RUN groupadd -r mysql && useradd -r -g mysql mysql
10//ENTRYPOINT docker run执行时，会覆盖CMD的命令，而ENTRYPOINT不会
ENTRYPOINT ["docker-entrypoint.sh"]
11//EXPOSE 指定镜像要暴露的端口，启动镜像时，可以使用-p将该端口映射给宿主机
EXPOSE 3306

```

### 步骤

1. 准备启动资源【jar】

2. 在docker环境中新建一个目录 first-dockerfile

3. 上传"xxx.jar"到该目录下，

4. 创建Dockerfile

5. 基于Dockerfile构建镜像   docker build -t second:v1.0 .  ` 最后有个点=用当前路径Dockerfile进行构建,镜像取名 second 并设定版本为 v1.0 `

6.  docker images 查看是否构建成功 

7. 基于image创建container  `docker run -d --name user01 -p 6666:8080 second`

8. 查看启动日志docker logs user01

9. 宿主机上访问curl localhost:6666/dockerfile

   

## 镜像仓库

### 官网

> hub.docker.com

```dockerfile
(1)在docker机器上登录
docker login
(2)输入用户名和密码
(3)docker push itcrazy2018/test-docker-image
[注意镜像名称要和ImageId一致，不然push不成功]
(4)给image重命名，并删除掉原来的
docker tag test-docker-image itcrazy2018/test-docker-image
docker rmi -f test-docker-image
(5)再次推送，刷新hub.docker.com后台，发现成功
(6)别人下载，并且运行
docker pull itcrazy2018/test-docker-image
docker run -d --name user01 -p 6661:8080 itcrazy2018/test-docker-image
```

### 阿里云

> https://account.aliyun.com/login/login.htm

```dockerfile
(1)登录到阿里云docker仓库
sudo docker login --username=用户名 registry.cnhangzhou.aliyuncs.com
(2)输入密码
(3)创建命名空间，比如itcrazy2016
(4)给image打tag
sudo docker tag [ImageId] registry.cn-hangzhou.aliyuncs.com/目录/ImageId:v1.0
(5)推送镜像到docker阿里云仓库
sudo docker push registry.cn-hangzhou.aliyuncs.com/目录/ImageId:v1.0
(6)别人下载，并且运行
docker pull registry.cn-hangzhou.aliyuncs.com/目录/ImageId:v1.0
docker run -d --name user01 -p 6661:8080 registry.cnhangzhou.aliyuncs.com/目录/ImageId:v1.0
```

## image

```dockerfile
(1)查看本地image列表
docker images
docker image ls
(2)获取远端镜像
docker pull
(3)删除镜像[注意此镜像如果正在使用，或者有关联的镜像，则需要先处理完]
docker image rm imageid
docker rmi -f imageid
docker rmi -f $(docker image ls) 删除所有镜像
(4)运行镜像
docker run image
(5)发布镜像
docker push
```

## container

> container是由image运行起来的 class-实体对象

container反向生成image ，不建议这样【mage怎么来的就全然不知】

> docker commit container名称 新的ImageId

### 资源限制

```dockerfile
//查看资源情况
docker stats
//内存限制 默认 其大小和memory一样
docker run -d --memory 100M --name tomcat1 tomcat
//CPU限制 --cpu-shares 权重
docker run -d --cpu-shares 10 --name tomcat2 tomcat
```

### 常见操作

```dockerfile
(1)根据镜像创建容器
docker run -d --name -p 9090:8080 my-tomcat tomcat
(2)查看运行中的container
docker ps
(3)查看所有的container[包含退出的]
docker ps -a
(4)删除container
docker rm containerid
docker rm -f $(docker ps -a) 删除所有container
(5)进入到一个container中
docker exec -it container bash
(6)根据container生成image
docker commit container名称 新的ImageId
(7)查看某个container的日志
docker logs container
(8)查看容器资源使用情况
docker stats
(9)查看容器详情信息
docker inspect container
(10)停止/启动容器
docker stop/start container
```

## 网络

```
查看网卡
ip a
ip link show
ls /sys/class/net
配置文件
cat /etc/sysconfig/network-scripts/ifcfg-eth0
给网卡添加IP地址
ip addr add 192.168.0.100/24 dev eth0
ip addr delete 192.168.0.100/24 dev eth0
重启网卡
service network restart / systemctl restart network
启动/关闭某个网卡
ifup/ifdown eth0 or ip link set eth0 up/down

```

### Network Namespace

```
不同的network namespace是互相隔离
ip netns list #查看
ip netns add ns1 #添加
ip netns delete ns1 #删除
查看该namespace下网卡的情况
ip netns exec ns1 ip a
启动ns1上的lo网卡
ip netns exec ns1 ip link set lo up
```

```
两个namespace网络连通
veth pair ：Virtual Ethernet Pair，是一个成对的端口
创建一对link
ip link add veth-ns1 type veth peer name veth-ns2
将veth-ns1加入ns1中，将veth-ns2加入ns2中
ip link set veth-ns1 netns ns1
ip link set veth-ns2 netns ns2
查看宿主机和ns1，ns2的link情况
ip link
ip netns exec ns1 ip link
ip netns exec ns2 ip link
添加ip
ip netns exec ns1 ip addr add 192.168.0.11/24 dev veth-ns1
ip netns exec ns2 ip addr add 192.168.0.12/24 dev veth-ns2
启动veth-ns1和veth-ns2
ip netns exec ns1 ip link set veth-ns1 up
ip netns exec ns2 ip link set veth-ns2 up
发现state是UP同时有IP
ip netns exec ns1 ip a
ip netns exec ns2 ip a
两个network namespace互相ping一下
ip netns exec ns1 ping 192.168.0.12
ip netns exec ns2 ping 192.168.0.11
```

### Container的网路

```
//安装一下
yum install bridge-utils
brctl show
查看docker中的网络模式 bridge也是docker中默认的网络模式
docker network ls
详情bridge
docker network inspect bridge
```

### 创建自己的network

```
创建一个network
docker network create tomcat-net
or
docker network create --subnet=172.18.0.0/24 tomcat-net
查看
docker network ls
查看详情信息
docker network inspect network名称
创建tomcat的容器，并且指定使用tomcat-net
docker run -d --name custom-net-tomcat --network tomcat-net tomcat
docker run -d --name container实例名 --network network名称 image名
查看网络信息
docker exec -it custom-net-tomcat ip a
查看网卡接口
brctl show

```

### 不同network中的container通信

```
//加入其他的network
docker network connect network名称 container名称
docker network connect tomcat-net tomcat01
查看tomcat-net网络，可以发现tomcat01这个容器也在其中
docker network inspect network名称
不仅可以通过ip地址ping通，而且可以通过名字ping到
```

### ip端口映射

```
端口映射
docker run -d --name port-tomcat -p 8090:8080 tomcat
```

```
centos7 ip映射的win10
#此时需要centos和win网络在同一个网段，所以在Vagrantfile文件中
#这种方式等同于桥接网络。也可以给该网络指定使用物理机哪一块网卡，比如
#config.vm.network"public_network",:bridge=>'en1: Wi-Fi (AirPort)'
config.vm.network"public_network"
centos7: ip a --->192.168.8.118
win10:浏览器访问 192.168.8.118:9080
```

```
centos7上的8090映射到win10的某个端口
#此时需要将centos7上的端口和win10上的端口做映射
config.vm.network"forwarded_port",guest:8098,host:8090
#记得vagrant reload生效一下
win10：浏览器访问 localhost：8098
```

## 数据持久化

### Volume

```
创建mysql数据库的container
docker run -d --name mysql01 -e MYSQL_ROOT_PASSWORD=jack123 mysql
查看volume
docker volume ls  
具体查看该volume
docker volume inspect
48507d0e7936f94eb984adf8177ec50fc6a7ecd8745ea0bc165ef485371589e8
名字不好看，name太长，修改一下 
docker run -d --name mysql01 -v mysql01_volume:/var/lib/mysql -e
MYSQL_ROOT_PASSWORD=jack123 mysql
```

### Bind Mounting

```
docker run -d --name tomcat01 -p 9090:8080 -v
/tmp/test:/usr/local/tomcat/webapps/test tomcat
查看两个目录 两个目录为共享
centos：cd /tmp/test
tomcat容器：cd /usr/local/tomcat/webapps/test
```


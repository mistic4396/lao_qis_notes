container 多个 =Pod

Pod 被ReplicaSet 管理，手段是selector

ReplicaSet 被Deployment 监控

Label 用于分类和selector 有关



ReplicationController

## 基本命令

```
vagrant halt 优雅关闭
vagrant up 正常启动


新建
kubectl apply -f nginx-pod.yaml
查看
kubectl get 【pods】 -o wide 
kubectl get pods -o wide
kubectl get rc
删除1
kubectl delete 【pods】 nginx-zzwzl
kubectl delete pods nginx-zzwzl
删除2
kubectl delete -f 【yaml名称】
kubectl delete -f nginx_replication.yaml

修改参数【对pod进行扩缩容】
kubectl scale 【rc】 【nginx名称】 --【replicas_key】=[vale]
kubectl scale rc nginx --replicas=5


```



## Controllers

### ReplicationController(RC)

```yml
apiVersion: v1
kind: ReplicationController   表示要新建对象的类型
metadata:
  name: nginx
spec:
  replicas: 3  Pod需要运行的副本数
  selector: 	管理的Pod的label
    app: nginx
  template:		Pod的模板
    metadata: 
      name: nginx 
      labels:		
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
```

### ReplicaSet(RS)

> RC的升级
>
> RC只支持基于等式的Label Selector
>
> RS支持基于集合的Label Selector

```yaml
apiVersion: extensions/v1beta1
kind: ReplicaSet
metadata:
  name: frontend
spec:
  matchLabels: 
    tier: frontend
  matchExpressions: 
    - {key:tier,operator: In,values: [frontend]}
  template:
  ...
```

### Deployment

> RC升级
>
> 可以随时知道当前Pod“部署”的进度

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

```
查看pod 
kubectl get pods -o wide

nginx-deployment[deployment]-
6dd86d77d[replicaset]-
f7dxb[pod] 

nginx-deployment-6dd86d77d-f7dxb   1/1     Running   0      22s   192.168.80.198   w2 
nginx-deployment-6dd86d77d-npqxj   1/1     Running   0      22s   192.168.190.71   w1 
nginx-deployment-6dd86d77d-swt25   1/1     Running   0      22s   192.168.190.70   w1


```

```
当前nginx的版本
kubectl get deployment -o wide

NAME    READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS   IMAGES      SELECTOR
nginx-deployment   3/3         3     3  3m27s      nginx    nginx:1.7.9   app=nginx
```

```
更新nginx的image版本
kubectl set image deployment nginx-deployment nginx=nginx:1.9.1
```

## Labels and Selectors

> 同一个label的pod，交给selector管理
>
> pod的label标签：kubectl get pods --show-labels

```yaml
apiVersion: apps/v1
kind: Deployment
metadata: 
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:             # 匹配具有同一个label属性的pod标签
    matchLabels:
      app: nginx         
  template:             # 定义pod的模板
    metadata:
      labels:
        app: nginx      # 定义当前pod的label属性，app为key，value为nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

## Namespace

> 命名空间就是为了隔离不同的资源【Pod、Service、Deployment】
>
> kubectl get pods   如果不指定，则使用默认的命名空间：default。
>
> kubectl get pods -n kube-system   指定命名空间`-n`
>
> kubectl get 【pods】 -n 【空间名】 
>
> kubectl get all -n myns   查看空间内的所以资源
>
> kubectl get pods --all-namespaces    #查找所有命名空间下的pod
>
> 
>
> 当前的命名空间：kubectl get namespaces/ns

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  namespace: myns  指定命名空间
spec:
  containers:
  - name: nginx-container
    image: nginx
    ports:
    - containerPort: 80
```

## Network

### 同一个Pod中的容器通信

> 同一个pod中的Container是共享网络ip地址和端口号的

### 集群内Pod之间的通信

> 可以相互访问

### 集群内Service-Cluster IP

> Pod是不稳定的，Pod的IP地址是变化的，需要固定的IP
>
> 把相同或者具有关联的Pod，打上Label，组成Service。而Service有固定的IP
>
> 访问是轮询的



```yaml
创建service
kubectl expose 【deployment】 【服务名称】
kubectl expose deployment whoami-deployment
yaml文件新建
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: MyApp  【选择器】
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
  type: Cluster  【集群】
```



```
查看service
kubect get svc
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   19h
查看详情
kubectl describe svc whoami-deployment
[root@master-kubeadm-k8s ~]# kubectl describe svc whoami-deployment
Name:              whoami-deployment 【名称】
Namespace:         default
Labels:            app=whoami 
Annotations:       <none>
Selector:          app=whoami 【选择器】
Type:              ClusterIP
IP:                10.105.147.59 【暴露地址】
Port:              <unset>  8000/TCP 【暴露端口】
TargetPort:        8000/TCP
Endpoints:         192.168.14.8:8000,192.168.221.81:8000,192.168.221.82:8000【关联的ip】
Session Affinity:  None
Events:            <none>
```

### Pod访问外部服务

> 直接访问即可

### 外部服务访问集群中的Pod

#### Service-NodePort

> Service的一种类型，可以通过NodePort的方式
>
> 在集群中每台物理机器上暴露一个相同的端口
>
> 问题：占用了各个物理主机上的端口

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: whoami-deployment
  labels:
    app: whoami
spec:
  replicas: 3
  selector:
    matchLabels:
      app: whoami
  template:
    metadata:
      labels:
        app: whoami
    spec:
      containers:
      - name: whoami
        image: jwilder/whoami
        ports:
        - containerPort: 8000
```



```yaml
type=NodePort
新建1
kubectl delete svc whoami-deployment
kubectl expose deployment whoami-deployment --type=NodePort
新建2 
apiVersion: v1
kind: Service
metadata:
  name: tomcat-service
spec:
  ports:
  - port: 80   	【暴露端口】
    protocol: TCP
    targetPort: 8080  	
  selector:
    app: tomcat    【选择器】
  type: NodePort  【类型】
  nodePort: 30001
```

```yaml
###hostNetwork：true ，Pod中的所有容器就直接暴露在宿主机的网络环境中,Pod的PodIP就是其所在Node的IP。
apiVersion: v1
kind: Pod
metadata:
  name: influxdb
spec:
  hostNetwork: true
  containers:
    - name: influxdb
      image: influxdb

```



```
查看
[root@master-kubeadm-k8s ~]# kubectl get svc
NAME                TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes          ClusterIP   10.96.0.1      <none>        443/TCP          21h
whoami-deployment   NodePort    10.99.108.82   <none>        8000:32041/TCP   7s

注意上述的端口32041，实际上就是暴露在集群中物理机器上的端口
lsof -i tcp:32041
netstat -ntlp|grep 32041
浏览器通过物理机器的IP访问
```



#### Service-LoadBalance

> 通常需要第三方云提供商支持，有约束性

#### Ingress

> 1 nginx ingress controller配置
>
> 2 定义ingress，service和pod
>
> 3 修改win的hosts文件，添加dns解析
>
> ​	192.168.8.61 tomcat.jack.com

```
# 确保nginx-controller运行到w1节点上
kubectl label node w1 name=ingress   

# 使用HostPort方式运行，需要增加配置
hostNetwork: true

# 搜索nodeSelector，并且要确保w1节点上的80和443端口没有被占用，镜像拉取需要较长的时间，这块注意一下哦
# mandatory.yaml在网盘中的“课堂源码”目录
kubectl apply -f mandatory.yaml  

kubectl get all -n ingress-nginx
查看w1的80和443端口
lsof -i tcp:80
lsof -i tcp:443
```



```yaml
#ingress
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: nginx-ingress
spec:
  rules:
  - host: tomcat.jack.com
    http:
      paths:
      - path: /
        backend:
          serviceName: tomcat-service
          servicePort: 80
```



```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tomcat-deployment
  labels:
    app: tomcat
spec:
  replicas: 1
  selector:
    matchLabels:
      app: tomcat
  template:
    metadata:
      labels:
        app: tomcat
    spec:
      containers:
      - name: tomcat
        image: tomcat
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: tomcat-service
spec:
  ports:
  - port: 80   
    protocol: TCP
    targetPort: 8080
  selector:
    app: tomcat
```



## Storage

### Volume

使用节点共享目录

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-pod
spec:
  containers:
  - name: nginx-container
    image: nginx
    ports:
    - containerPort: 80
    volumeMounts:		 【volume使用】
    - name: volume-pod
      mountPath: /nginx-volume
  - name: busybox-container
    image: busybox
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
    volumeMounts:		【volume使用】
    - name: volume-pod
      mountPath: /busybox-volume
  volumes:				【volume定义】
  - name: volume-pod
    hostPath:
      path: /tmp/volume-pod 
```

### PersistentVolume

> volume的plugin实现，是K8s中的资源，生命周期独立于Pod

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  capacity:      # 存储空间大小
    storage: 5Gi    
  volumeMode: Filesystem
  accessModes:       # 请求模式，只允许一个Pod进行独占式读写操作
    - ReadWriteOnce     
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: slow
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /tmp            # 远端服务器的目录
    server: 172.17.0.2    # 远端的服务器
```

### PersistentVolumeClaim

> PVC来绑定PV【是根据size storage和访问模式 accessModes 进行匹配】，PVC交给Pod

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:		
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
```

- 创建PVC时指定了accessModes = ReadWriteOnce。这表明这个PVC希望使用accessModes = ReadWriteOnce的PV。`
- 创建PVC时指定了volumeMode= Filesystem。这表明这个PVC希望使用volumeMode= Filesystem的PV。
- 创建PVC时指定了`storageClassName: slow`，此配置用于绑定PVC和PV，意思是这个PVC希望使用`storageClassName=slow`的PV。我们可以看到最上面创建PV时也包含`storageClassName=slow`的配置。
- PVC还可以指定PV必须满足的Label，如加了selector匹配`matchLabels: release: "stable"`。这表明此PVC希望使用Label：`release: "stable"`的PV。
- 最后是resources声明，跟pod一样可以声明使用特定数量的资源，`storage: 8Gi`表明此PVC希望使用8G的Volume资源。

   通过上面的分析，我们可以看到PVC和PV的绑定，不是简单的通过Label来进行。而是要综合storageClassName，accessModes，matchLabels以及storage来匹配符合条件的PV进行绑定。

### Pod中如何使用PVC

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```

### StorageClass

> StorageClass声明存储插件，用于自动创建PV。
>
> PV的模板，其中有两个重要部分：PV属性和创建此PV所需要的插件。
>
> 这样PVC就可以按“Class”来匹配PV。

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: standard
provisioner: kubernetes.io/aws-ebs
parameters:
  type: gp2
reclaimPolicy: Retain
allowVolumeExpansion: true
mountOptions:
  - debug
volumeBindingMode: Immediate
```

有了StorageClass之后的PVC可以变成这样

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
name: test-claim1
spec:
accessModes:
    - ReadWriteMany
resources:
 requests:
    storage: 1Mi
  storageClassName: nfs
```

- provisioner：指定 Volume 插件的类型

  > 内置插件 ：kubernetes.io/glusterfs、kubernetes.io/aws-ebs
  >
  > 外部插件：ceph.com/cephfs

- parameters：指定 provisioner 的选项，比如 glusterfs 支持 resturl、restuser 等参数。

- reclaimPolicy：指定回收策略，同 PV 的回收策略。

- mountOptions：指定挂载选项，当 PV 不支持指定的选项时会直接失败。比如 NFS 支持 hard 和 nfsvers=4.1 等选项。

  > Pod中使用只需要像使用volumes一样，指定名字就可以
  >
  > Pod想要使用共享存储，创建PVC描述了想要什么类型的后端存储，匹配对应的PV，没有匹配成功，Pod就会处于Pending状态
  >
  > 一个Pod可以使用多个PVC，一个PVC也可以给多个Pod使用
  >
  > 一个PVC只能绑定一个PV，一个PV只能对应一种后端存储
  >
  > 对于PV或者StorageClass只能对应一种后端存储

- PV的状态

Available：表示当前的pv没有被绑定

Bound：表示已经被pvc挂载

Released：pvc没有在使用pv, 需要管理员手工释放pv

Failed：资源回收失败

- PV回收策略

Retain：表示删除PVC的时候，PV不会一起删除，而是变成Released状态等待管理员手动清理

Recycle：在Kubernetes新版本就不用了，采用动态PV供给来替代

Delete：表示删除PVC的时候，PV也会一起删除，同时也删除PV所指向的实际存储空间

## Pod

### Lifecycle

- 挂起（Pending）：Pod 已被 Kubernetes 系统接受，但有一个或者多个容器镜像尚未创建。等待时间包括调度 Pod 的时间和通过网络下载镜像的时间，这可能需要花点时间。
- 运行中（Running）：该 Pod 已经绑定到了一个节点上，Pod 中所有的容器都已被创建。至少有一个容器正在运行，或者正处于启动或重启状态。
- 成功（Succeeded）：Pod 中的所有容器都被成功终止，并且不会再重启。
- 失败（Failed）：Pod 中的所有容器都已终止了，并且至少有一个容器是因为失败终止。也就是说，容器以非0状态退出或者被系统终止。
- 未知（Unknown）：因为某些原因无法取得 Pod 的状态，通常是因为与 Pod 所在主机通信失败

### 重启策略

- Always：容器失效时，即重启
- OnFailure：容器终止运行且退出码不为0时重启
- Never:永远不重启



### 静态Pod

静态Pod是由kubelet进行管理的，并且存在于特定的Node上。

不能通过API Server进行管理，无法与ReplicationController,Ddeployment或者DaemonSet进行关联，也无法进行健康检查。

### 健康检查

LivenessProbe探针：判断容器是否存活

ReadinessProbe探针：判断容器是否启动完成

## ConfigMap

#### 创建

创建命令行

```
# 创建一个名称为my-config的ConfigMap，key值时db.port，value值是'3306'
kubectl create configmap my-config --from-literal=db.port='3306'
kubectl get configmap
```

配置文件创建

```
创建一个文件，名称为app.properties
name=jack
age=17
kubectl create configmap app --from-file=./app.properties
kubectl get configmap
kubectl get configmap app -o yaml
```

目录中创建

```
mkdir config
cd config
mkdir a
mkdir b
cd ..

kubectl create configmap config --from-file=config/
kubectl get configmap
```

yaml文件创建

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: special-config
  namespace: default
data:
  special.how: very
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: env-config
  namespace: default
data:
  log_level: INFO
```

```
kubectl apply -f configmaps.yaml
kubectl get configmap
```

#### 使用

> ConfigMap必须在Pod使用它之前创建
>
> 使用envFrom时，将会自动忽略无效的键
>
> Pod只能使用同一个命名空间的ConfigMap

通过环境变量使用

> 关键字：valueFrom--》configMapKeyRef--》name
>
> configMap

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: dapi-test-pod
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "env" ]  #使用环境变量
      env:     # 声明环境变量
       
        - name: SPECIAL_LEVEL_KEY   # 需要配置的key
          valueFrom:		  # 关键字
            configMapKeyRef:  # 关键字
          
              name: special-config #  configMap的名字
              
              key: special.how      #  configMap里的key
  restartPolicy: Never
```

用作命令行参数

> 先设置为环境变量,之后可以用过$(VAR_NAME)设置容器启动命令的启动参数

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: dapi-test-pod2
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "echo $(SPECIAL_LEVEL_KEY)" ]
      env:
        - name: SPECIAL_LEVEL_KEY
          valueFrom:
            configMapKeyRef:
              name: special-config
              key: special.how
  restartPolicy: Never
```

作为volume挂载使用

> 将创建的ConfigMap直接挂载至Pod的/etc/config目录下，其中每一个key-value键值对都会生成一个文件
>
> key为文件名，value为内容

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-configmap2
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "ls /etc/config/" ]
      volumeMounts:
      - name: config-volume
        mountPath: /etc/config
  volumes:
    - name: config-volume
      configMap:
        name: special-config
  restartPolicy: Never
```

## Secret

Secret类型

- Opaque：使用base64编码存储信息，可以通过`base64 --decode`解码获得原始数据，因此安全性弱。
- kubernetes.io/dockerconfigjson：用于存储docker registry的认证信息。
- kubernetes.io/service-account-token：用于被 serviceaccount 引用。serviceaccout 创建时 Kubernetes 会默认创建对应的 secret。Pod 如果使用了 serviceaccount，对应的 secret 会自动挂载到 Pod 的 /run/secrets/kubernetes.io/serviceaccount 目录中。

### Opaque Secret

> Opaque类型的Secret的value为base64位编码后的值

#### 从文件中创建

```
echo -n "admin" > ./username.txt
echo -n "1f2d1e2e67df" > ./password.txt
kubectl create secret generic db-user-pass --from-file=./username.txt --from-file=./password.txt
kubectl get secret
```

#### yaml文件创建

> echo -n 'admin' | base64
> echo -n '1f2d1e2e67df' | base64

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  username: YWRtaW4=
  password: MWYyZDFlMmU2N2Rm
```

```
kubectl create -f ./secret.yaml
kubectl get secret
kubectl get secret mysecret -o yaml
```

#### Secret使用

> 以Volume方式，新建目录
>
> 以环境变量方式，valueFrom-->secretKeyRef->Secret[名称]-->key[name]

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mypod
    image: redis
    volumeMounts:
    - name: foo
      mountPath: "/etc/foo"
      readOnly: true
  volumes:
  - name: foo
    secret:
      secretName: mysecret
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: secret-env-pod
spec:
  containers:
  - name: mycontainer
    image: redis
    env:
      - name: SECRET_USERNAME
        valueFrom:
          secretKeyRef:
            name: mysecret
            key: username
      - name: SECRET_PASSWORD
        valueFrom:
          secretKeyRef:
            name: mysecret
            key: password
  restartPolicy: Never
```

### dockerconfigjson

> kubernetes.io/dockerconfigjson用于存储docker registry的认证信息，可以直接使用`kubectl create secret`命令创建

### ervice-account-token

> 用于被 serviceaccount 引用。
>
> serviceaccout 创建时 Kubernetes 会默认创建对应的 secret。Pod 如果使用了 serviceaccount，对应的 secret 会自动挂载到 Pod 的 /run/secrets/kubernetes.io/serviceaccount 目录中。

```
kubectl get secret   # 可以看到service-account-token
kubectl run nginx --image nginx
kubectl get pods
kubectl exec -it nginx-pod-name bash
ls /run/secrets/kubernetes.io/serviceaccount
```

```
kubectl get secret
kubectl get pods pod-name -o yaml   
#  找到volumes选项，定位到-name，secretName
#  找到volumeMounts选项，定位到mountPath: /var/run/secrets/kubernetes.io/serviceaccount
```

```
无论是ConfigMap，Secret，还是DownwardAPI，都是通过ProjectedVolume实现的，可以通过APIServer将信息放到Pod中进行使用。
```

## 指定Pod所运行的Node

给node打上label

> kubectl get nodes
> kubectl label nodes worker02-kubeadm-k8s name=jack

查看node是否有上述label

> kubectl describe node worker02-kubeadm-k8s

部署一个mysql的pod

>  nodeSelector: 
>
> ​       name: jack

查看pod运行详情

> kubectl apply -f mysql-pod.yaml
> kubectl get pods -o wide

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: mysql-rc
  labels:
    name: mysql-rc
spec:
  replicas: 1
  selector:
    name: mysql-pod
  template:
    metadata:
      labels: 
        name: mysql-pod
    spec:
      nodeSelector: 
        name: jack
      containers:
      - name: mysql
        image: mysql
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 3306
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: "mysql"
      
---
apiVersion: v1
kind: Service
metadata:
  name: mysql-svc
  labels: 
    name: mysql-svc
spec:
  type: NodePort
  ports:
  - port: 3306
    protocol: TCP
    targetPort: 3306
    name: http
    nodePort: 32306
  selector:
    name: mysql-pod
```

## Job & CronJob

> Job负责批量处理短暂的一次性任务，仅执行一次，并保证处理的一个或者多个Pod成功结束。
>
> cronJob是基于时间进行任务的定时管理。

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: job-demo
spec:
  template:
    metadata:
      name: job-demo
    spec:
      restartPolicy: Never
      containers:
      - name: counter
        image: busybox
        command:
        - "bin/sh"
        - "-c"
        - "for i in 9 8 7 6 5 4 3 2 1; do echo $i; done"
```

## StatefulSet

> StatefulSet可以看作是Deployment/RC对象的特殊变种
>
> - StatefulSet里的每个Pod都有稳定、唯一的网络标识，可以用来发现集群内其他的成员
> - Pod的启动顺序是受控的，操作第n个Pod时，前n-1个Pod已经是运行且准备好的状态
> - StatefulSet里的Pod采用稳定的持久化存储卷，通过PV/PVC来实现，删除Pod时默认不会删除与StatefulSet相关的存储卷
> - StatefulSet需要与Headless Service配合使用

```yaml
# 定义Service
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: web
  clusterIP: None
  selector:
    app: nginx
---
# 定义StatefulSet
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
spec:
  selector:
    matchLabels:
      app: nginx 
  serviceName: "nginx"  
  replicas: 3 
  template:
    metadata:
      labels:
        app: nginx 
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
          name: web
```

## DaemonSet

> 在每个节点上运行 

## Horizontal Pod Autoscaler

> 自动地根据观察到的CPU利用率,自动地缩放

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
name: nginx-deployment
labels:
app: nginx
spec:
replicas: 3
selector:
matchLabels:
app: nginx
template:
metadata:
labels:
  app: nginx
spec:
containers:
      - name: nginx
     image: nginx
     ports:
        - containerPort: 80
```

```
# 使nginx pod的数量介于2和10之间，CPU使用率维持在50％
kubectl autoscale deployment nginx-deployment --min=2 --max=10 --cpu-percent=50
```

```
查看所有创建的资源
kubectl get pods
kubectl get deploy
kubectl get hpa
```

```
01-控制管理器每隔30s查询metrics的资源使用情况
02-通过kubectl创建一个horizontalPodAutoscaler对象，并存储到etcd中
03-APIServer:负责接受创建hpa对象，然后存入etcd
```

## Resource和Dashboard

> requests 请求多少
>
> limits  最大

```yaml
apiVersion: v1
kind: Pod
metadata:
name: frontend
spec:
containers:
  - name: db
 image: mysql
 env:
    - name: MYSQL_ROOT_PASSWORD
   value: "password"
 resources:
   requests:
     memory: "64Mi"     # 表示64M需要内存
     cpu: "250m"        # 表示需要0.25核的CPU
   limits:
     memory: "128Mi"    
     cpu: "500m"
  - name: wp
 image: wordpress
 resources:
   requests:
     memory: "64Mi"
     cpu: "250m"
   limits:
     memory: "128Mi"
     cpu: "500m"
```

Dashboard

> kubectl apply -f dashboard.yaml
>
> kubectl get pods -n kube-system
> kubectl get pods -n kube-system -o wide
> kubectl get svc -n kube-system
> kubectl get deploy kubernetes-dashboard -n kube-system

```
使用火狐浏览器访问
https://121.41.10.13:30018/
```

```shell
生成登录需要的token
# 创建service account
kubectl create sa dashboard-admin -n kube-system

# 创建角色绑定关系
kubectl create clusterrolebinding dashboard-admin --clusterrole=cluster-admin --serviceaccount=kube-system:dashboard-admin

# 查看dashboard-admin的secret名字
ADMIN_SECRET=$(kubectl get secrets -n kube-system | grep dashboard-admin | awk '{print $1}')
echo ADMIN_SECRET

# 打印secret的token
kubectl describe secret -n kube-system ${ADMIN_SECRET} | grep -E '^token' | awk '{print $2}'
```

